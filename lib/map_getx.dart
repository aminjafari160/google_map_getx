import 'dart:async';

import 'package:fluster/fluster.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:map_example/map_helper.dart';
import 'package:map_example/map_marker.dart';

class MapController extends GetxController {
 
  int count = 0;
  // variable
  List<LatLng> _latlangs = [
    LatLng(32.654171, 51.667455),
    LatLng(32.654171, 51.667455),
    LatLng(32.652929, 51.665937),
    LatLng(32.651844, 51.665064),
    LatLng(32.651844, 51.665064),
    LatLng(32.650101, 51.666080),
    LatLng(32.654879, 51.654307),
    LatLng(32.654879, 51.654307),
  ];

  List<LatLng> get getlatlangs => _latlangs;

  set setlatlangs(LatLng latlangs) {
    _latlangs.add(latlangs);
  }

  final Completer<GoogleMapController> _mapControllerq = Completer();
  PolylinePoints polylinePoints = PolylinePoints();
  List<LatLng> polylineCoordinates = [];
  Set<Marker> _marker = {};
  GoogleMapController _controller;
  Set<Polyline> _polylines = {};
  LatLng marker;
  List<MapMarker> mapMarkers = [];

  List<Marker> _googleMarkers = [];

  List<Marker> get googleMarkers => _googleMarkers;

  set googleMarkers(List<Marker> googleMarkers) {
    _googleMarkers = googleMarkers;
  }

  Set<Marker> get getmarker => _marker;

  Location location = new Location();
  LatLng _currentLocation;

  // getter
  LatLng get currentLocation => _currentLocation;

  Set<Polyline> get polylines => _polylines;

  GoogleMapController get controller => _controller;

  // setter
  set setMarker(LatLng value) {
    count++;
    marker = value;
    _marker.add(
      Marker(
        markerId: MarkerId("marker$count"),
        position: value,
      ),
    );
    update();
  }


  // methode
  void getCurrentLocation() {
    location.onLocationChanged.listen(
      (l) {
        _currentLocation = LatLng(l.latitude, l.longitude);
        // cameraPositioned(_currentLocation);
      },
    );
  }

  void cameraPositioned(LatLng position) {
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: position,
          zoom: 18,
        ),
      ),
    );
  }


  getPolyline() async {
    polylineCoordinates.clear();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      "AIzaSyA-tyTpjB3Szvnlcv-BQSPXNX5QqfZAH6M",
      PointLatLng(currentLocation.latitude, currentLocation.longitude),
      PointLatLng(marker.latitude, marker.longitude),
      travelMode: TravelMode.walking,
    );
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
    _addPolyLine();
  }

  _addPolyLine() {
    PolylineId id = PolylineId("poly");
    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.red,
      width: 5,
      points: polylineCoordinates,
    );

    polylines.add(polyline);
    update();
  }
// map

  final String _markerImageUrl =
      'https://img.icons8.com/office/80/000000/marker.png';

  /// Minimum zoom at which the markers will cluster
  final int _minClusterZoom = 0;

  /// Maximum zoom at which the markers will cluster
  final int _maxClusterZoom = 19;

  /// [Fluster] instance used to manage the clusters
  Fluster<MapMarker> _clusterManager;

  double _currentZoom = 15;

  bool _isMapLoading = true;

  /// Markers loading flag
  bool _areMarkersLoading = true;

  void onMapCreated(GoogleMapController controller) {
    // _mapControllerq.complete(controller);

    // _isMapLoading = false;

    _initMarkers();
  }

  /// Inits [Fluster] and all the markers with network images and updates the loading state.
  void _initMarkers() async {
    final List<MapMarker> markers = [];

    for (LatLng markerLocation in _latlangs) {
      final BitmapDescriptor markerImage =
          await MapHelper.getMarkerImageFromUrl(_markerImageUrl);

      markers.add(
        MapMarker(
          id: _latlangs.indexOf(markerLocation).toString(),
          position: markerLocation,
          icon: BitmapDescriptor.defaultMarker,
        ),
      );
    }

    _clusterManager = await MapHelper.initClusterManager(
      markers,
      _minClusterZoom,
      _maxClusterZoom,
    );

    await updateMarkers();
  }

  /// Gets the markers and clusters to be displayed on the map for the current zoom level and
  /// updates state.
  Future<void> updateMarkers([double updatedZoom]) async {
    if (_clusterManager == null || updatedZoom == _currentZoom) return;

    if (updatedZoom != null) {
      _currentZoom = updatedZoom;
    }

    final updatedMarkers = await MapHelper.getClusterMarkers(
      _clusterManager,
      _currentZoom,
      Colors.red,
      Colors.white,
      80,
    );

    _marker
      ..clear()
      ..addAll(updatedMarkers);
    update();
  }
}
