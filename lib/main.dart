import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:map_example/map_getx.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:map_example/model_address.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Icon(Icons.map),
              Icon(Icons.add),
            ],
          ),
        ),
        body: TabBarView(children: [
          Map(),
          GeoCoder(),
        ]),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerTop,
      ),
    );
  }
}

class Map extends StatefulWidget {
  @override
  _MapState createState() => _MapState();
}

class _MapState extends State<Map> {
  MapController _mapController = Get.put(MapController());

  CameraPosition _cameraPosition = CameraPosition(
    bearing: 192.8334901395799,
    target: LatLng(37.43296265331129, -122.08832357078792),
    tilt: 59.440717697143555,
    zoom: 10,
  );
  @override
  void initState() {
    _mapController.getCurrentLocation();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MapController>(
      init: MapController(),
      builder: (context) => GoogleMap(
        mapToolbarEnabled: false,
        initialCameraPosition: _cameraPosition,
        markers: _mapController.getmarker,
        onMapCreated: (controller) => _mapController.onMapCreated(controller),
        onCameraMove: (position) => _mapController.updateMarkers(position.zoom),
      ),
    );
  }
}

class GeoCoder extends StatefulWidget {
  @override
  _GeoCoderState createState() => _GeoCoderState();
}

class _GeoCoderState extends State<GeoCoder> {
  final kGoogleApiKey = "AIzaSyCZoLFKzcg-pM5kTDoAF-uHuZLC8UinJhc";


  Future autocomplete() async {
    http.Response a = await http.get(
        "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=1069 td+Amsterdam&key=$kGoogleApiKey&sessiontoken=1234567890");
    Addresss address = Addresss.fromJson(jsonDecode(a.body));
    geocoder(address.predictions.first.structuredFormatting.mainText);
    print(address.predictions.first.structuredFormatting.mainText);
  }

  Future geocoder(String query) async {
    // From a query
// final query = "1600 Amphiteatre Parkway, Mountain View";
    var addresses =
        await Geocoder.google(kGoogleApiKey).findAddressesFromQuery(query);
    var first = addresses.first;
    print("${first.featureName} : ${first.coordinates}");
  }

  String str = "";
  TextEditingController _textEditingController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          TextField(
            controller: _textEditingController,
            decoration: InputDecoration(labelText: "insert postal code"),
          ),
          SizedBox(
            height: 50,
          ),
          Text(str)
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          return autocomplete();
          //  geocoder(_textEditingController.text);
        },
        child: Icon(Icons.search),
      ),
    );
  }
}
